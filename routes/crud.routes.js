

module.exports = (app) => {
    const cruds = require('../controllers/crud.controller.js');

    app.post('/crud-create', cruds.create);

    app.get('/crud/create', cruds.createNew);

    app.get('/', cruds.findAll);

    app.get('/cruds-edit/:crudId', cruds.findOne);

    app.post('/crud-update', cruds.update);

    app.get('/crud-delete/:crudId', cruds.delete);
}
