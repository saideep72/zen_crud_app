const Crud = require('../models/crud.model.js');


exports.create = (req, res) => {
  console.log(req.body);
    const crud = new Crud({
        name: req.body.name,
        type: req.body.type,
        favorite: req.body.favorite
    });

    crud.save()
    .then(data => {
       res.redirect('/');
    }).catch(err => {
        res.redirect('/error_404');
    });
};



exports.findAll = (req, res) => {

  Crud.find()
  .then(crud => {

      res.render('crud-all',{data:crud});

  }).catch(err => {
      res.redirect('/error_404');
  });



};

exports.createNew = (req, res) => {

      res.render('crud-create')



};

exports.findOne = (req, res) => {

    Crud.findById(req.params.crudId)
    .then(crud => {

        res.render('crud-edit',{data:crud});

    }).catch(err => {
        res.redirect('/error_404');
    });
};

exports.update = (req, res) => {

    Crud.findByIdAndUpdate(req.body.id, {

      name: req.body.name,
      type: req.body.type,
      favorite: req.body.favorite

    }, {new: true})
    .then(crud => {
        res.redirect('/');
    }).catch(err => {
      res.redirect('/error_404');
    });

};


exports.delete = (req, res) => {
    Crud.findByIdAndRemove(req.params.crudId)
    .then(crud => {
        if(!crud) {
          res.redirect('/error_404');
        }
        res.redirect('/');
    }).catch(err => {
      res.redirect('/error_404');
    });
};
