const mongoose = require('mongoose');


const CrudSchema = mongoose.Schema({
    name: String,
    type: String,
    favorite: String
}, {
    timestamps: true
});

//CrudSchema.plugin(mongooseAggregatePaginate);
module.exports = mongoose.model('Crud', CrudSchema);
